import  sai_phn_num as spn
import unittest as ut

class Testphn_num(ut.TestCase):
    def test1(self):
        self.assertEqual(spn.phn_num('123-456-7890'), True)

    def test2(self):
        self.assertEqual(spn.phn_num('123 456 7890'), True)

    def test3(self):
        self.assertEqual(spn.phn_num('(123)456-7890'), True)

    def test4(self):
        self.assertEqual(spn.phn_num('1234567890'), True)

    def test5(self):
        self.assertEqual(spn.phn_num('1 123 456-7890'), True)

    def test6(self):
        self.assertEqual(spn.phn_num('1 123 456 7890'), True)

    def test7(self):
        self.assertEqual(spn.phn_num('(789)4561324'), True)

    def test8(self):
        self.assertEqual(spn.phn_num('1325681234'), True)

    def test9(self):
        self.assertEqual(spn.phn_num('132568123'), False)

    def test10(self):
        self.assertEqual(spn.phn_num('123456789'), False)

    def test11(self):
        self.assertEqual(spn.phn_num('132568123 456 7890'), False)

    def test12(self):
        self.assertEqual(spn.phn_num('0 (123)789 4567'), False)

    def test13(self):
        self.assertEqual(spn.phn_num('abc 132 1234'), False)

    def test14(self):
        self.assertEqual(spn.phn_num('(789)456132456'), False)

    def test15(self):
        self.assertEqual(spn.phn_num('1 123 25 123'), False)

    def test16(self):
        self.assertEqual(spn.phn_num('7 132 4567 1234'), False)


if __name__ == '__main__':
    ut.main()



#----------------------------------------------------------------------
#Ran 16 tests in 0.001s

#OK
