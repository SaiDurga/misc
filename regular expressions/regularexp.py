import re

def match_email(email):
    if re.match('[\w]+\.?[\w]*[+]?([\w\.]*)?@[\w]+\.([a-z]+\.)?[a-z]+$', email):
        return True
    return False

#--------------------------------------------------------------------------

# matching files

def match_files(file):
    if re.match('[\w]+\.(jpg|png|gif)$', file):
        return True
    return False


#----------------------------------------------------------------------
# trimming whitespaces

def spaces(text):
    return re.sub('^\s*|\s*$', '', text)

#-------------------------------------------------------------------------

def log_file(logentry):
    return re.search('(\w+)\(([\w+\.\w]+):(\d+)', logentry).groups()


#--------------------------------------------------------------------------
# Parsing and extracting data from a URL
def data_url(url):
    return re.search('(\w+)://([\w\.\-]+):?(\d+)?', url).groups()


#-------------------------------------------------------------------------

#matchinh html tags

def match_tag(link):
    return re.search('<(\w+)[>\s][\w\W]*', link).groups()
