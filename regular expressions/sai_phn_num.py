import re

def phn_num(num):
    if re.match('\d?[\s(]?(\d{3})[\s\-\)]?\d{3}[\s\-]?\d{4}$', num):
        return True
    return False
