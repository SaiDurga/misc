
import  sai_numbers as sn
import unittest as ut

class Testreg_exp(ut.TestCase):
    def test1(self):
        self.assertEqual(sn.reg_exp('1.2e568'), True)

    def test2(self):
        self.assertEqual(sn.reg_exp('123456'), True)

    def test3(self):
        self.assertEqual(sn.reg_exp('1.8'), True)

    def test4(self):
        self.assertEqual(sn.reg_exp('1.82345'), True)

    def test5(self):
        self.assertEqual(sn.reg_exp('0.012e34'), True)

    def test6(self):
        self.assertEqual(sn.reg_exp('-0.012'), True)

    def test7(self):
        self.assertEqual(sn.reg_exp('1.0e0000.2'), True)

    def test8(self):
        self.assertEqual(sn.reg_exp('e12345'), True)

    def test9(self):
        self.assertEqual(sn.reg_exp('-136.0'), True)

    def test10(self):
        self.assertEqual(sn.reg_exp('-18000000022323'), True)

    def test11(self):
        self.assertEqual(sn.reg_exp('18000000022323'), True)


    def test12(self):
        self.assertEqual(sn.reg_exp('p1234'), False)

    def test13(self):
        self.assertEqual(sn.reg_exp('120p'), False)

    def test14(self):
        self.assertEqual(sn.reg_exp('120cd'), False)

    def test15(self):
        self.assertEqual(sn.reg_exp('abc234'), False)

    def test16(self):
        self.assertEqual(sn.reg_exp('a123'), False)

    def test17(self):
        self.assertEqual(sn.reg_exp('a12c34d'), False)

    def test18(self):
        self.assertEqual(sn.reg_exp('136d12'), False)

    def test19(self):
        self.assertEqual(sn.reg_exp('p132'), False)

    def test20(self):
        self.assertEqual(sn.reg_exp('abc123'), False)

    def test21(self):
        self.assertEqual(sn.reg_exp('abcdef'), False)


if __name__ == '__main__':
        ut.main()


# python -m unittest sai_numbers_testcase.py
.....................
----------------------------------------------------------------------
#Ran 21 tests in 0.001s

#OK
