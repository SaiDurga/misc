import re

def reg_exp(num):
    if re.match('[+-]?e?\d+[\.e+-]?\d*?[,e+-\.]?[\d]*[,e\.]?[\d]+$', num):
        return True
    return False
