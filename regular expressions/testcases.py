import  regularexp as reg
import unittest as ut

#test cases for emails
class Testmatch_email(ut.TestCase):
    def test1(self):
        self.assertEqual(reg.match_email('sai.chirumamilla@gmail.com'), True)

    def test2(self):
        self.assertEqual(reg.match_email('kollisupriya5@gmail.com'), True)

    def test3(self):
        self.assertEqual(reg.match_email('saidurga.chirumamilla@technoidentity.com'), True)

    def test4(self):
        self.assertEqual(reg.match_email('example@abc.org'), True)

    def test5(self):
        self.assertEqual(reg.match_email('saidurga.ch@hotmail.com'), True)

    def test6(self):
        self.assertEqual(reg.match_email('saidurga.ch@gmail.eu.com'), True)

    def test7(self):
        self.assertEqual(reg.match_email('saidurga.....@gmail.com'), True)

    def test8(self):
        self.assertEqual(reg.match_email('133456'), False)

    def test9(self):
        self.assertEqual(reg.match_email('sai+durga.+chirumamilla@gmail.com'), False)

    def test10(self):
        self.assertEqual(reg.match_email('sai+durga+chirumamilla@gmail.com'), False)

    def test11(self):#no whitespaces
        self.assertEqual(reg.match_email('sai durga.chirumamilla@gmail.com'), False)

    def test12(self):
        self.assertEqual(reg.match_email('sai.ch@gmail.com12'), False)

# test cases for matching files(jpg, png, gif)
    def test13(self):
        self.assertEqual(reg.match_files('abc.png'), True)

    def test14(self):
        self.assertEqual(reg.match_files('sai12.png'), True)

    def test15(self):
        self.assertEqual(reg.match_files('123.png'), True)

    def test16(self):
        self.assertEqual(reg.match_files('test.gif'), True)

    def test17(self):
        self.assertEqual(reg.match_files('sd32.jpg'), True)

    def test18(self):
        self.assertEqual(reg.match_files('htm.html'), False)

    def test19(self):
        self.assertEqual(reg.match_files('sai.jpg.png'), False)

    def test20(self):
        self.assertEqual(reg.match_files('@#8.jpg'), False)

    def test21(self):
        self.assertEqual(reg.match_files('sai durga.gif'), False)

    def test22(self):
        self.assertEqual(reg.match_files('sai#.jpg'), False)

#test cases for removing whitespaces

    def test23(self):
        self.assertEqual(reg.spaces('    saidurga'), 'saidurga')

    def test24(self):
        self.assertEqual(reg.spaces('                sai durga chirumamilla'), 'sai durga chirumamilla')

    def test25(self):
        self.assertEqual(reg.spaces('saidurga    '), 'saidurga')

    def test26(self):
        self.assertEqual(reg.spaces('  sai durga'), 'sai durga')

    def test27(self):
        self.assertEqual(reg.spaces('sai durga '), 'sai durga')

    def test28(self):
        self.assertEqual(reg.spaces('saidurga'), 'saidurga')

    def test29(self):
        self.assertEqual(reg.spaces('    sai durga       chirumamilla    '), 'sai durga       chirumamilla')


# test cases for extracting methodname, filename and line number from log entry files

    def test30(self):
        self.assertEqual(reg.log_file('E/( 1553): at widget.List.makeView(ListView.java:1727)'), ('makeView', 'ListView.java', '1727'))

    def test31(self):
        self.assertEqual(reg.log_file('E/( 1553):   at widget.List.fillDown(ListView.java:652)'), ('fillDown', 'ListView.java', '652'))

    def test32(self):
        self.assertEqual(reg.log_file('E/( 1553): at widget.List.fillFrom(ListView.java:709)'), ('fillFrom', 'ListView.java', '709'))

# test cases for gettinh protocol, host and port number from an url

    def test33(self):
        self.assertEqual(reg.data_url('ftp://file_server.com:21/top_secret/life_changing_plans.pdf'), ('ftp', 'file_server.com', '21') )

    def test34(self):
        self.assertEqual(reg.data_url('https://regexone.com/lesson/introduction#section'), ('https', 'regexone.com', None))

    def test35(self):
        self.assertEqual(reg.data_url('file://localhost:4040/zip_file'), ('file', 'localhost', '4040'))

    def test36(self):
        self.assertEqual(reg.data_url('https://s3cur3-server.com:9999/'),('https', 's3cur3-server.com', '9999') )

    def test37(self):
        self.assertEqual(reg.data_url('market://search/angry%20birds'), ('market', 'search', None) )

    def test38(self):
        self.assertEqual(reg.data_url('https://technoidentity.slack.com/messages/G7KSZ52JX/'),  ('https', 'technoidentity.slack.com', None))

    def test39(self):
        self.assertEqual(reg.data_url('http://localhost:8888/tree'), ('http', 'localhost', '8888') )

    def test40(self):
        self.assertEqual(reg.data_url('https://gitlab.com/SaiDurga/mlassignments'), ('https', 'gitlab.com', None) )

if __name__ == '__main__':
    ut.main()


    #----------------------------------------------------------------------
#Ran 40 tests in 0.003s

#OK
