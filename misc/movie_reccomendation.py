
# coding: utf-8

# In[2]:

import numpy as np
from numpy import nan
def pearson(x,y):
    z1=(x-np.mean(x))/(np.std(x))
    z2=(y-np.mean(y))/(np.std(y))
    return (np.sum(z1*z2))/(np.size(x))

def read_ratings(file_name):
    return np.loadtxt(fname=file_name,skiprows=1,dtype=float,delimiter=',',unpack=True)

def norm(x,min,max):
    a=np.asarray(x)
    return (a-min)/(max-min)

def norm_imdb(x):
    return norm(x,0,10)

def norm_rt(x):
    return norm(x,0,10)

def norm_rv(x):
    return norm(x,0,4)

def norm_re(x):
    return norm(x,0,4)

def norm_mc(x):
    return norm(x,0,10)

def norm_tg(x):
    return norm(x,0,5)

def norm_ac(x):
    return norm(x,0,4)

def norm_ratings(file_name):
    ratings=read_ratings(file_name).T
    ratings[:,1]=norm_imdb(ratings[:,1])
    ratings[:,2]=norm_rt(ratings[:,2])
    ratings[:,3]=norm_rv(ratings[:,3])
    ratings[:,4]=norm_re(ratings[:,4])
    ratings[:,5]=norm_mc(ratings[:,5])
    ratings[:,6]=norm_tg(ratings[:,6])
    ratings[:,7]=norm_ac(ratings[:,7])
    return ratings

def missing(a, b):
    c = np.stack((a,b),axis=-1)
    c1=np.isnan(c)
    d= np.all(~c1, axis=1)
    return pearson(a[d],b[d])

def pearson_ratings(filename,k):
    ratings =norm_ratings(filename).T
    arr=[missing(ratings[k],ratings[j]) for j in range(1,7)]
    return arr

p=np.array(pearson_ratings(('movie_rating.csv'),7))
#p1=np.reshape(np.round_(p,decimals=3),(7,7))
p


# ## recommending the movie with 'average formula'

# In[17]:

def movie_recommend_with_formula(file_name):
    ratings=norm_ratings(file_name)
    a=np.isnan(ratings)
    d=np.all(~a, axis=1)
    d1=ratings[~d]
    ka=d1[:,1:7]
    kb=d1[:,0]
    k1=np.multiply(p,ka)
    k2=np.sum(k1,axis=1)
    k3=np.sum(p)
    k4=k2/k3
    k5=np.stack((kb,k4),axis=-1)
    k6=max(k4)
    k7=np.any(k5==k6,axis=1)
    return k5[k7]


# ## directly finding the recommended movie!

# In[153]:

def pearson_ratings(filename):
    ratings =norm_ratings(filename).T
    arr=np.array([missing(ratings[7],ratings[j]) for j in range(1,7)])
    k1=np.argmax(arr)
    k2=ratings[k1+1]
    temp = ratings[0]
    k3=np.stack((temp,k2,ratings[7]),axis=-1)
    k4=np.isnan(k3)
    k5=k3[np.any(k4, axis=1)]
    k6=np.argmax(k5[:,1])
    return k5[k6,0]

pearson_ratings('movie_rating.csv')

