
# coding: utf-8

# In[1]:

def matrix_add(lst1,lst2):
    m1 = len(lst1)
    n1 = len(lst1[0])
    m2 = len(lst2)
    n2 = len(lst2[0])
    res = []
    if m1 == m2 and n1 == n2 :
        for i in range(0,m1):
            res1 = []
            for j in range(0,n1):
                res1.append(lst1[i][j]+lst2[i][j])
            res.append(res1)
    else:
        print("matrix addition not possible")
    return res



# In[2]:

matrix_add(([1,2,3,4],[4,5,6,7]),([1,2,3],[4,5,6]))


# In[3]:

matrix_add(([1,2,3],[4,5,6]),([1,2,3],[4,5,6]))


# In[4]:

def matrix_sub(lst1,lst2):
    m1 = len(lst1)
    n1 = len(lst1[0])
    m2 = len(lst2)
    n2 = len(lst2[0])
    res = []
    if m1 == m2 and n1 == n2 :
        for i in range(0,m1):
            res1 = []
            for j in range(0,n1):
                res1.append(lst1[i][j]-lst2[i][j])
            res.append(res1)
    else:
        print("matrix subtraction not possible")
    return res


# In[5]:

matrix_sub(([4,5,6],[2,1,9]),([3,1,5],[4,5,6]))


# In[6]:

def matrix_mul(lst1,lst2):
    m1 = len(lst1)
    n1 = len(lst1[0])
    m2 = len(lst2)
    n2 = len(lst2[0])
    res = []
    if n1 == m2:
        for i in range(0,m1):
            res1 = []
            for j in range(0,n2):
                sum = 0
                for k in range(0,m2):
                    sum += (lst1[i][k]*lst2[k][j])    
                res1.append(sum)
            res.append(res1)
    else:
        print('matrix multiplication is not possible')
    return res
    


# In[7]:

matrix_mul(([1,2,3],
            [4,5,6],
            [7,8,9],
            [1,1,1]),([2,6],
                      [7,1],
                      [9,1]))


# In[8]:

matrix_mul(([1,2],[3,4]),([2,3,4],[5,6,7],[3,8,9]))


# In[9]:

def matrix_transpose(lst):
        n = len(lst)
        m = len(lst[0])
        res = []
        for i in range(m):
            res1 =[]
            for j in range(n):
                res1.append(lst[j][i])
            res.append(res1)
        return res

matrix_transpose(([1,3,4],
                  [5,6,5],))


# In[11]:

def det2(lst):
    m = len(lst[0])
    n = len(lst)
    if m == 2 and n == 2:
        i = 0
        j = 1
        res = lst[i][i]*lst[j][j]-lst[i][j]*lst[j][i]
    return res

det2(([1,2],[3,7]))                 


# In[ ]:

def matrix_det(lst):
    m = len(lst[0])
    n = len(lst)
    if m == n and m > 2 and n > 2:
        if det2(lst) != 0:
            
            
            
        else:
            print('matrix is not valid')
    
    


# In[ ]:

def matrix_inverse(lst):
    m = len(lst[0])
    n = len(lst)
    res = []
    for i in range(m):
            res1 =[]
            for j in range(n):
                res1.append

