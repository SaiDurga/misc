
# coding: utf-8

# In[2]:

x=[8,16,29,33,41,57,66,72,80,99]


# In[3]:

y=[9,17,28,34,40,58,65,73,81,98]


# In[4]:

sub=lambda x,y :x-y


# In[5]:

def manhatt_dist(x,y):
    return sum(map(abs,map(sub,x,y)))


# In[6]:

manhatt_dist(x,y)


# In[7]:

sqrt=lambda x:x**0.5


# In[8]:

sqr=lambda x:x**2


# In[9]:

def eucl_dist(x,y):
    return sqrt(sum(map(sqr,map(sub,x,y))))


# In[10]:

eucl_dist(x,y)


# In[11]:

def cheby_dist(x,y):
    return max(map(abs,map(sub,x,y)))


# In[12]:

cheby_dist(x,y)

