
# coding: utf-8

# In[4]:

def even(x):
        if x % 2  ==0:
            return True
        else:
            return False
    
even(5)


# In[5]:

def odd(x):
        if not x% 2  ==0:
            return True
        else:
            return False
        
    
odd(5)


# In[6]:

def filter1(pred, lst):
    filt = []
    for i in lst:
        if pred(i): 
            filt.append(i)
    return filt

filter1(even,range(1,10))


# In[8]:

def inc(a):
    a += 1
    return a

inc(5)


# In[9]:

def map1(f):
    def mapped(lst):
        result = []
        for i in lst:
            result.append(f(i))
        return result
    return mapped

map1(inc)(range (1,12))


# In[10]:

def add(x,y):
    return x + y

add(9,3)


# In[11]:

def mul(x,y):
    return x*y

mul(33,2)


# In[12]:

def reduce(bf, init):
    def reduced(lst):
        result = init
        for e in lst:
            result = bf(result, e)
        return result
    return reduced

reduce(add,2)([1,2,3,4,5])


# In[13]:

reduce(mul,2)([1,2,3,4])


# In[14]:

def take(a):
    def taked(lst): 
        res = []
        for i in range(a):
            res.append(lst[i]) 
        return res
    return taked
    
take(5)([4,99,7,9,0, 5,1,8,44])   


# In[15]:

def take_while(pred):
    def take_while1(lst):
        res = []
        for i in lst:
            if pred(i): 
                res.append(i)
            else:
                return res
        return res
    return take_while1

take_while(even)([2,4,10,1,6,10,11])


# In[41]:

def drop(a,lst): 
    if a < 0:
        return lst
    return lst[a:]

drop(0,[1,2,3,4,5,6,7])
    


# In[17]:

def drop_while(pred,lst):
    for e in range(0, len(lst)):
        if pred(lst[e]):
            if not pred(lst[e+1]):
                return (lst[e+1:])
        else:
            return lst
    
drop_while(odd,[2,3,4])


# In[18]:

def split_at(n, lst):
    return (take(n)(lst),drop(n, lst))

split_at(7, [1,2,3,4,5,6,7])


# In[19]:

def split_with(n, lst):
    return (take_while(n)(lst),drop_while(n, lst))

split_with(odd, [1,3,2,3,4,5,6,7])


# In[20]:

def reverse(lst):
    sol = []
    for a in range(1, len(lst)+1):
        sol.append(lst[-a])
    return sol

reverse([1,2,3,4])


# In[21]:

def distinct(lst):
    r = []
    for a in lst:
        if a not in r:
            r.append(a)
    return r   
distinct([1,2,3,4,4,5,3,2,4,1,6,8,3,7,3,8,6,1,11,84,2,75,22,8,8,11,24,2,9,9])


# In[22]:

def dedupe(lst):
    sol = []
    for a in range(len(lst)):
        if lst[a] != lst[a-1]:
            sol.append(lst[a])
    return sol
       

dedupe([1,1,4,4,2,2,2,6,6,7,2,9,9,1,2,9])


# In[23]:

def concat(lst,lst1):
    res = []
    for i in  lst:
        res.append(i)
    for j in lst1:
        res.append(j)
    return res

concat([1],[4])


# In[24]:

def frequency(lst):
    freq = {}
    for e in lst:
        freq[e] = freq.get(e, 0) + 1
    return freq
frequency([1,1,2,4,6,2,6,2,7,2,4,1,6,17,33,3,8,3,7,33,1])    


# In[25]:

def concat1(lst,a):
    res = []
    for i in  lst:
        res.append(i)
    res.append(a)
    return res

concat1([1,2,3],23)


# In[26]:

def index(lst):
    res = {}
    for i in range(len(lst)):
        res[lst[i]]= concat1(res.get(lst[i],[]),i)
    return res

index([1,4,2,3,5,7,6,5,1,7,9,2])


# In[125]:

def partioin(n, lst):
    res = []
    a = range(len(lst)+1)
    a = a[n: : n]
    print(list(a))
    for i in range(len(a)):
        for j in range(len(lst)):
            if i == 0:
                x = 0
            else:
                x = a[i-1]
            if a[i]-1 == j:
                res1 = lst[x:j+1]
                res.append(res1)
    return res

partioin(3,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14])        
            
            


# In[28]:

def partition_by(pred, lst):
    res = []
    res1 = []
    for i in lst:
        res1.append(i)
        if pred(i):
            res1.pop()
            if res1 != []:
                res.append(res1)
            res.append([i])
            res1 =[]
    return res

def valx(i):
    def val(j):
        if i == j:
            return True
    return val


# In[29]:

partition_by(odd, [3,1, 2,4, 7, 3,6, 5, 7, 8, 7,2,7,6,8,6,5])


# In[30]:

val7 = valx(7)
partition_by(val7, [3,1, 2,4, 7, 3,6, 5, 7, 8, 7,2,7,6,8,6,5])


# In[31]:

def square(coll):
    res = []
    for e in coll:
        res.append(e*e)
    return res

square([1,2,3,4])


# In[32]:

def mapcat(f, coll):
    res = []
    for e in coll:
        res = concat(res,map1(f)(e))
    return res

mapcat(inc, [[2,1],[4,3],[6,5]])


# In[33]:

def mapcat1(f, coll):
    res = []
    for e in coll:
        res = concat(res,f(e))
    return res

mapcat1(square, [[2,1],[4,3],[6,5]])


# In[34]:

def min1(lst1, lst2):
    if len(lst1) < len(lst2):
        return len(lst1)
    else:
        return len(lst2)

min1([1,2,3],[4,5])        
    


# In[36]:

def interleave(lst1, lst2):
    res = []
    for i in range(min1(lst1, lst2)):
        res.append(lst1[i])
        res.append(lst2[i])
    return res
            
interleave([1,2,3,5,0,8], "abc")


# In[37]:

def interpose(lst1, lst2):
    res = []
    for i in range(len(lst2)):
        res.append(lst2[i])
        if i < len(lst2)-1:
            res.append(lst1)
    return res
            
interpose("s",[1,2,3,5,0,8])


# In[38]:

def sort(lst):
    for i in range(len(lst)):
        for j in range(len(lst)):
            if lst[i]<lst[j]:
                [lst[i], lst[j]] = [lst[j], lst[i]]
    return lst

sort([5,2,9,7,0])


# In[105]:




# In[133]:

def partioin_all(n, lst):
    result = []
    arr = range(len(lst))
    arr = arr[n: : n]
    print(list(arr))
    for i in range(len(arr)):
        for j in range(len(lst)):
            if i == 0:
                x = 0
            else:
                x = arr [i-1]
            if arr[i]-1 == j:
                res1 = lst[x:j+1]
                result.append(res1)
    while arr[-1] == lst[-1]:
        result.append(lst)
    return result

partioin_all(3,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14])        
            
            


# In[132]:

arr = [1,2,3,4]
print(arr[-1])

