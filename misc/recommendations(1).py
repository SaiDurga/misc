
# coding: utf-8

# In[21]:

import numpy as np
def pearson(x,y):
    z1=(x-np.mean(x))/(np.std(x))
    z2=(y-np.mean(y))/(np.std(y))
    return (np.sum(z1*z2))/(np.size(x))
def read_ratings(file_name):
    return np.loadtxt(fname=file_name,skiprows=1,dtype=float,delimiter=',',unpack=True)
def norm(x,min,max):
    a=np.array(x)
    return (a-min)/(max-min)
def norm_imdb(x):
    return norm(x,0,10)
def norm_rt(x):
    return norm(x,0,10)
def norm_rv(x):
    return norm(x,0,4)
def norm_re(x):
    return norm(x,0,4)
def norm_mc(x):
    return norm(x,0,10)
def norm_tg(x):
    return norm(x,0,5)
def norm_ac(x):
    return norm(x,0,4)
def norm_ratings(file_name):
    ratings=read_ratings(file_name).T
    ratings[:,1]=norm_imdb(ratings[:,1])
    ratings[:,2]=norm_rt(ratings[:,2])
    ratings[:,3]=norm_rv(ratings[:,3])
    ratings[:,4]=norm_re(ratings[:,4])
    ratings[:,5]=norm_mc(ratings[:,5])
    ratings[:,6]=norm_tg(ratings[:,6])
    ratings[:,7]=norm_ac(ratings[:,7])
    return ratings

def missing(a, b):
    c = np.stack((a,b),axis=-1)
    d= np.all(np.any([c > 0],axis=0), axis=1)
    return pearson(a[d],b[d])

def pearson_ratings(filename):
    ratings =norm_ratings(filename).T
    arr=[missing(ratings[i],ratings[j]) for i in range(1,7) for j in range(1,7)]
    
    return arr


# In[22]:

p=np.array(pearson_ratings('movie_ratings.csv'))
p1=np.reshape(np.round_(p,decimals=5),(6,6))
p1


# In[26]:

p2=max(p1[5])
p3=max(i for i in p1[5] if i!=p2)
p3

