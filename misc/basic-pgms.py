
# coding: utf-8

# In[3]:

def armstrong(num):
    sum=0
    temp = num
    while temp > 0:
        digit = temp % 10
        sum += digit*digit*digit
        temp //= 10
    if num == sum: 
        print (num,"is an armstrong no")
    else:
        print (num, "is not an armstrong no")
        
armstrong(1634)


# In[2]:

def power(x, n):
    num = x
    if n == 0:
        return 1
    elif n==1:
        return x
    else:
        for i in range(1, n):
            x = x * num
        return x
power(2,5)


# In[4]:

def no_of_digits(num):
    i=0
    temp = num
    while temp > 0:
        digit = temp % 10
        temp //= 10
        i = i+1
    return i
no_of_digits(82992)


# In[5]:

def armstrong1(num):
    sum=0
    temp = num
    n = no_of_digits(num)
    while temp > 0:
        digit = temp % 10
        sum += power(digit, n)
        temp //= 10
    if num == sum: 
        return True
    else:
        return False

armstrong1(1364)


# In[6]:

def perfect_numbers(start,stop):
    for n in range(start,stop):
        sum=0
        for i in range(1,n):
            if n%i == 0:
                sum = sum+i
        if sum == n:
            print(n)
perfect_numbers(1,100)


# In[7]:

def prime_numbers(start,stop):
    for n in range(start,stop):
        k = 0
        for i in range(2,n):
            if n%i == 0:
                k = k + 1
        if k<=0:
            print(n)
prime_numbers(1,10)


# In[8]:

def sum1(lst):
    x=0
    for i in lst:
        x += i
    return x
sum1([1,2,3,4])


# In[9]:

def mean(lst):
    result =0
    for i in range (1,len(lst)):
        result = (sum1(lst))/(len(lst))
    return result
    
mean([1,2,3,4])


# In[10]:

def sub(x):
    def square(y):
        return (x-y)**2
    return square

sub(5)(7)


# In[11]:

def variance(lst):
    res =0
    for i in lst:
        res = res + sub(mean(lst))(i)
        result = res/len(lst)
    return result
variance([1,2,3,4,5,6,7])


# In[12]:

def sqrt (x):
    res = x **(1/2)
    return res

sqrt(2)


# In[13]:

def standard_deviation(lst):
    result = sqrt(variance(lst))
    return result

standard_deviation([1,2,3,4,5,6,7])


# In[14]:

def median(lst):
    pos=0
    lst = sorted(lst)
    print(lst)
    if not len(lst)%2 == 0:
        pos =(len(lst)-1)//2
        print(lst[pos])
    else:
        pos= len(lst)//2
        result = (lst[pos-1]+lst[pos])/2
        print(result)
    
median([1,3,2,4,12,7,9,11])


# In[15]:

def fibo(n):
    result = [1,1]
    while len(result) < n:
        result.append(result[-1] + result[-2])
    return result[:n]

fibo(5)


# In[16]:

def inc(x):
    return x+1
inc(3)


# In[18]:

def pipe(first, *rest):
    def piped(*args):
        result=first(*args)
        for f in rest:
            result=f(result)
        return result
    return piped

pipe(inc,inc,inc,str)(100)

