
# coding: utf-8

# In[1]:

def even(x):
        if x % 2  ==0:
            return True
        else:
            return False
    
even(5)


# In[2]:

def odd(x):
        if not x% 2  ==0:
            return True
        else:
            return False
        
    
odd(5)


# In[89]:

def filter1(pred, gen):
    for i in gen:
        if pred(i): 
            yield i
list(filter1(even,range(10)))


# In[4]:

def inc(a):
    a += 1
    return a

inc(5)


# In[70]:

def map1(f):
    def mapped(gen):
        for i in gen:
            yield f(i)
    return mapped

list(map1(inc)(range (1,10)))


# In[14]:

def add(x,y):
    return x + y

add(9,3)


# In[35]:

def mul(x,y):
    return x*y

mul(33,2)


# In[90]:

def take(a):
    def taked(gen): 
        res = []
        for i in range(a):
            yield gen[i]
    return taked
list(take(5)(range(20)))   


# In[91]:

def take_while(pred):
    def take_while1(gen):
        for i in gen:
            if pred(i): 
                yield i
            else:
                return gen
    return take_while1

list(take_while(even)(range(10)))


# In[92]:

def drop(a,gen): 
    if a < 0:
        return gen
    for i in range(a,len(gen)):
        yield gen[i]
list(drop(3,[1,2,3,4,5,6,7]))    


# In[93]:

def drop_while(pred,gen):
    for e in range(0, len(gen)):
        if pred(gen[e]):
            if not pred(gen[e+1]):
                for i in range(e+1, len(gen)):
                    yield gen[i]
                break
        else:
            return gen    
list(drop_while(odd,range(1,10)))


# In[94]:

def reverse(gen):
    for a in range(1, len(gen)+1):
        yield gen[-a]
list(reverse([1,2,3,4]))


# In[95]:

def dedupe(gen):
    for a in range(len(gen)):
        if gen[a] != gen[a-1]:
            yield gen[a]
list(dedupe([1,1,4,4,2,2,2,6,6,1,2,9]))


# In[96]:

def square(gen):
    for e in gen:
        yield e*e
list(square([1,2,3,4]))


# In[97]:

def concat(gen,gen1):
    res = []
    for i in  gen:
        yield i
    for j in gen1:
        yield j
list(concat(range(4),range(9,14)))


# In[100]:

def concat1(gen,a):
    for i in  gen:
        yield i
    yield a
list(concat1(range(6),23))


# In[110]:

def min1(gen,gen1):
    if len(gen) < len(gen1):
        return len(gen)
    else:
        return len(gen1)

min1(range(6),range(10))


# In[112]:

def interleave(gen1,gen2):
    for i in range(min1(gen1, gen2)):
        yield gen2[i]
        yield gen1[i]
list(interleave(range(7), "abc"))


# In[114]:

def interpose(gen, gen1):
    for i in range(len(gen1)):
        yield gen1[i]
        if i < len(gen1)-1:
            yield gen
list(interpose("s",range(10)))

