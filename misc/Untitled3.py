
# coding: utf-8

# In[4]:

a = 7
print(a)


# In[6]:

type(a)


# In[9]:

type(58)


# In[11]:

type(58.9)


# In[13]:

type('abc')


# In[14]:

5


# In[16]:

0666


# In[17]:

1+1


# In[19]:

123-12


# In[20]:

6373*2


# In[22]:

24/2


# In[23]:

24//2


# In[24]:

1+2+282+223


# In[25]:

1+23-363+3526


# In[26]:

53//0


# In[47]:

a = 13
a


# In[48]:

a //= 4
a


# In[32]:

temp = a-1
temp


# In[39]:

a += 172
a


# In[42]:

a //= 1
a


# In[49]:

divmod(9,5)


# In[51]:

2 + 3 * 4


# In[52]:

0b10


# In[54]:

0o10


# In[56]:

0x10


# In[57]:

int(True)


# In[59]:

int(False)


# In[60]:

int(637.28)


# In[61]:

int(1.0e4)


# In[62]:

int('-23')


# In[63]:

int('99 bottles of beer on the wall')


# In[65]:

googol = 10**100
googol


# In[66]:

googol * googol


# In[67]:

float(True)


# In[68]:

float(False)


# In[69]:

float(98)


# In[70]:

float('1.0e4')


# In[71]:

"Crackle"


# In[72]:

'A "two by four" is actually 1 1⁄2" × 3 1⁄2".'


# In[73]:

'''Boom!'''


# In[76]:

poem = '''There was a Young Lady of Norway,
Who casually sat in a doorway;
When the door squeezed her flat,
She exclaimed, "What of that?"
This courageous Young Lady of Norway.'''
poem


# In[77]:

print(99, 'bottles', 'would be enough.')


# In[78]:

''''''


# In[79]:

bottles = 99
base = ''
base += 'current inventory: '
base += str(bottles)
base


# In[80]:

str(1.0e4)


# In[82]:

palindrome = 'A man,\nA plan,\nA canal:\nPanama.'
print(palindrome)


# In[83]:

print('ab\tc')


# In[85]:

testimony = "\"I did nothing!\" he said. \"Not that either! Or the other thing.\""
testimony


# In[86]:

speech = 'Today we honor our friend, the backslash: \\.'
speech


# In[87]:

'Release the kraken! ' + 'At once!'


# In[88]:

"My word! " "A gentleman caller!"


# In[89]:

a = 'Duck.'
b = a
c = 'Grey Duck!'
a + b + c


# In[90]:

print(a, b, c)


# In[92]:

letters = 'abcdefghijklmnopqrstuvwxyz'
letters[-1]


# In[93]:

letters[100]


# In[95]:

name = 'Henny'
name[0] = 'P'


# In[96]:

name = 'Henny'
name.replace('H', 'P')


# In[97]:

letters[:]


# In[98]:

letters[20:]


# In[99]:

letters[12:15]


# In[100]:

letters[-3:]


# In[101]:

letters[18:-3]


# In[102]:

letters[-6:-2]


# In[103]:

letters[::7]


# In[104]:

letters[4:20:3]


# In[105]:

letters[-1::-1]


# In[106]:

letters[::-1]


# In[107]:

letters[70::70]


# In[108]:

len(letters)


# In[109]:

todos = 'get gloves,get mask,give cat vitamins,call ambulance'
todos.split(',')


# In[111]:

crypto_list = ['Yeti', 'Bigfoot', 'Loch Ness Monster']
crypto_string = ', '.join(crypto_list)
print('Found and signing book deals:', crypto_string)


# In[112]:

poem = '''All that doth flow we cannot liquid name
Or else would fire and water be the same;
But that is liquid which is moist and wet
Fire that property can never get.
Then 'tis not cold that doth the fire put out
But 'tis the wet that makes it die, no doubt.'''
poem[:13]


# In[113]:

len(poem)


# In[114]:

poem.startswith('All')


# In[115]:

poem.endswith('That\'s all, folks!')


# In[116]:

word = 'the'
poem.find(word)


# In[117]:

poem.isalnum()


# In[119]:

setup = 'a duck goes into a bar...'
setup.strip('.')


# In[120]:

setup.title()


# In[121]:

setup.capitalize()


# In[122]:

setup.upper()


# In[123]:

setup.lower()


# In[124]:

setup.center(30)


# In[126]:

setup.rjust(30)


# In[127]:

setup.replace('duck', 'marmoset')


# In[ ]:



