import unittest
import distance

class pearson_corr_test(unittest.TestCase):

    def test1(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70,79]
        self.assertEqual(distance.pearson_corr(v1,v2),0.32961238803118825)

    def test2(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70,79]
        self.assertEqual(distance.pearson_corr(v1,v2),0.82961238803118825)

    def test3(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70]
        with self.assertRaises(IndexError):
            distance.pearson_corr(v1,v2)
    



    

if __name__ == '__main__':
    unittest.main()
