import unittest
import distance

class manhattan_test(unittest.TestCase):

    def test1(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70,79]
        self.assertEqual(distance.manh_dist(v1,v2),127)

    def test2(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70,79]
        self.assertEqual(distance.manh_dist(v1,v2),120)

    def test3(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70]
        with self.assertRaises(IndexError):
            distance.manh_dist(v1,v2)


    



    

if __name__ == '__main__':
    unittest.main()
