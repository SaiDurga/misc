def euclid_distance(v1,v2):
    res = []
    result = []
    #assert(len(v1) == len(v2))
    for e in range(len(v1)):
        res.append((v1[e]-v2[e])**2)
    result = (add(res))**0.5
    return result


def add(lst):
    s = 0
    for e in lst:
        s = s+e
    return s

def mean(lst):
    return  add(lst)/(len(lst))

def mul(lst1,lst2):
    res = []
    for e in range(len(lst1)):
        res.append(lst1[e]*lst2[e])
    return res

def stan_dev(lst):
    squ=[]
    for e in lst:
        squ.append(e*e)
    return (mean(squ)-(mean(lst)**2))**0.5

def manh_dist(v1,v2):
    res1 = []
    res = []
    #assert(len(v1) == len(v2))
    for e in range(len(v1)):
        res1.append(abs(v1[e]-v2[e]))
    res= add(res1)
    return res


def pearson_corr(v1,v2):
    #assert(len(v1)==len(v2))
    cov = mean(mul(v1,v2))-(mean(v1))*(mean(v2))
    res = cov/(stan_dev(v1)*stan_dev(v2))
    return res

def chebyshev_distance(v1,v2):
    res1 = []
    #assert(len(v1)==len(v2))
    for e in range(len(v1)):
        res1.append(abs(v1[e]-v2[e]))
    return max(res1)

