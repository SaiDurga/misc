import unittest
import distance

class chebyshev_distance_test(unittest.TestCase):

    def test1(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70,79]
        self.assertEqual(distance.chebyshev_distance(v1,v2),39)

    def test2(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70,79]
        self.assertEqual(distance.chebyshev_distance(v1,v2),3)

    def test3(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70]
        with self.assertRaises(IndexError):
            distance.chebyshev_distance(v1,v2)
if __name__ == '__main__':
    unittest.main()
