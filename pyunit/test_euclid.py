import unittest
import distance

class Euclidian_test(unittest.TestCase):

    def test1(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70,79]
        self.assertEqual(distance.euclid_distance(v1,v2),61.08191221630181)

    def test2(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70,79]
        self.assertEqual(distance.euclid_distance(v1,v2),68.08191221630181)

    def test3(self):
        v1 = [63,68,86,72,73,74]
        v2 = [87,100,110,111,70]
        with self.assertRaises(IndexError):
            distance.euclid_distance(v1,v2)

if __name__ == '__main__':
    unittest.main()
