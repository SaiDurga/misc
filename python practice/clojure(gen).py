def map(f,gen):
    for e in gen:
        yield f(e)
        
m = map(even,range(10))
for i in m:
    print(i)

o/p:
 0
None
2
None
4
None
6
None
8
None

-------------------------------------------------------------------------------------------------------------------------------

            ##filter##

def filter(pred,gen):
    for e in gen:
        if pred(e):
            yield e
f = filter(even,range(10))      
for i in f:
    print(i)

o/p:
2
4
6
8


---------------------------------------------------------------------------------------------------------------------------------------

                  ##reduce##

def sum(lst):
    s = 0
    for e in lst:
        s = s+e
    return s

def reduce(bf, init, gen):
    result =init
    for e in gen:
        result = bf([result,e])
    return result

reduce(sum,0,range(11))

o/p: 55

------------------------------------------------------------------------------------------------------------------------------------------

            ##distinct##

def distinct(gen):
    res = []
    for e in gen:
        if e not in res:
            res.append(e)
    return res

distinct([1,2,3,1,2,3,4,5,4,5])

o/p : [1, 2, 3, 4, 5]

--------------------------------------------------------------------------------------------------------------------------------------------

		##dedupe##

def dedupe(gen):
    l = len(gen)
    for i in range(0,l):
        if (gen[i] != gen[i-1]) :
            yield(gen[i])

d = dedupe([1,2,3,1,1,2,2,3,3,1,2,3])
for i in d:
    print(i)

o/p:
1
2
3
1
2
3
1
2
3

----------------------------------------------------------------------------------------------------------------------------------------------

		##concat##

def concat(gen1,gen2):
    for e in gen1:
        yield(e)
    for e in gen2:
        yield(e)

c = concat([1,2,3,4],[4,[6,7],8])
for i in c:
    print(i)

o/p:
1
2
3
4
4
[6, 7]
8

---------------------------------------------------------------------------------------------------------------------------------------------

		##flatten##

def flatten(gen1,gen2):
    con  = concat(gen1,gen2)
    for e in con:
        if type(e) == list:
            for i in e:
                yield(i)
        else:
            yield(e)

f = flatten([1,2,3,4],[5,[6,7],8])
for i in f:
    print(i)

o/p:
1
2
3
4
5
6
7
8
------------------------------------------------------------------------------------------------------------------------------------------

		##mapcat##

def mapcat(f,gen1,gen2):
    for e in gen1:
        if type(e) == list:
            for i in e:
                yield(f(i))
        else:
            yield(f(e))
    for e in gen2:
        if type(e) == list:
            for i in e:
                yield(f(i))
        else:
            yield(f(e))

m = mapcat(inc,([1,2,3]),([4,[5,6],7]))
for i in m:
    print(i)

o/p:
2
3
4
5
6
7
8

--------------------------------------------------------------------------------------------------------------------------------------------

		##take##

def take(n,gen):
    if n>0 :
        for i in range(n):
            yield(gen[i])

g = take(5,[1,2,3,4,5,6])
for i in g:
    print(i)

o/p:
1
2
3
4
5

--------------------------------------------------------------------------------------------------------------------------------------------

		##interleave##

def interleave(gen1,gen2):
    l1 = len(gen1)
    l2 = len(gen2)
    if l1<l2 :
        l = l1
    else:
        l = l2
    result = []
    for i in range(0,l):
        yield(gen1[i],gen2[i])


il = interleave([1,2,3,7],[4,5,6,8,9,10])
for i in il:
    print(i)

o/p:
(1, 4)
(2, 5)
(3, 6)
(7, 8)

------------------------------------------------------------------------------------------------------------------------------------------

		##takewhile##

def takewhile(f,gen):
    for e in gen:
        if f(e):
            yield(e)
        else:
            break


tw = takewhile(even,[2,3,4,5])
for i in tw:
    print(i)

o/p:
2

---------------------------------------------------------------------------------------------------------------------------------------

		##partition##

def partition(n,gen):
    for i in range(0,(len(gen)//n)):
        yield(gen[(i*n):((i+1)*n)])
        
p = partition(4,[1,2,3,4,5,6])
for i in p:
    print(i)

o/p:
[1, 2, 3, 4]

------------------------------------------------------------------------------------------------------------------------------------------

		##partition-all##

def partition_all(n,gen):
    for i in range(0,(len(gen)//n+1)):
        yield(gen[(i*n):((i+1)*n)])
                
pa = partition_all(4,[1,2,3,4,5,6])
for i in pa:
    print(i)

o/p:
[1, 2, 3, 4]
[5, 6]


---------------------------------------------------------------------------------------------------------------------------------------------

		##reverse##

def reverse(gen):
    l = len(gen)
    for i in range(0,l):
        yield(gen[(l-1)-i])
        
r = reverse([1,2,3,4])
for i in r:
    print(i)

o/p:
4
3
2
1

--------------------------------------------------------------------------------------------------------------------------------------------


		##splitat##

def splitat(n,gen):
    gen1= []
    gen2= []
    if n  in gen:
        i = gen.index(n)
        for e in range(i+1):
            gen1.append(gen[e])
        for e in range(i+1,len(gen)):
            gen2.append(gen[e])
    else:
        gen1 = gen
    yield(gen1,gen2)

sa = splitat(4,[1,2,3,4,5])
for i in sa:
    print(i)

o/p:
([1, 2, 3, 4], [5])


---------------------------------------------------------------------------------------------------------------------------------------------



