def fac(n):
    fact = 1
    for i in range(1,n+1):
        fact = fact * i
    return fact

def ncr(n,r):
    return (fac(n))/((fac(r))*(fac(n-r))) 

def pascal_line(n):
    arr = []
    for i in range(0,n+1):
        arr.append(ncr(n,i))
    return arr

def pascal_triangle(n):
    arr = []
    for i in range(0,n+1):
        arr.append(pascal_line(i))
    return arr

pascal_triangle(3)
