def mean(lst):
    sum = 0
    for e in lst:
        sum = plus(sum,e)
    result = sum/(len(lst))
    return result

def square(lst):
    squared = []
    for e in lst:
        squared.append(e*e)
    return squared

def sqrt(x):
    return x**(1/2)

def variance(lst):
    m = mean(lst)
    diff = []
    for e in lst:
        diff.append(m-e)
    squ = square(diff)
    var = mean(squ)
    print(var)
    return sqrt(var)
-------------------------------------------------------

def sub(x):
    def square(y):
        return (x-y)**2
    return square

def variance2(lst):
    m = mean(lst)
    s = 0
    for e in lst:
        s = s + sub(m)(e)
    var = s/(len(lst))
    print('var=',var)
    sd = var**(1/2)
    print('sd=',sd)
