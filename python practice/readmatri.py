def matrix(row,column,lst):
    mat = []
    for i in range(0,row):
        mat1 = []
        for j in range(0,column):
            mat1.append(lst[j+(i*column)])
        mat.append(mat1)
    return mat


matrix(3,3,[1,2,3,4,5,6,7,8,9])

o/p:   [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

matrix(3,4,[1,2,3,4,5,6,7,8,9,10,11,12])

o/p:   [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]

matrix(2,2,[1,3,6,7])

o/p:  [[1, 3], [6, 7]]

matrix(2,3,[1,2,3,4,5])

o/p:  invalid number of elements

matrix(2,3,[1,2,3,4,5,6])

o/p:  [[1, 2, 3], [4, 5, 6]]
