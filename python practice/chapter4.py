#Compare with if, elif, and else

disaster = True
if disaster:
    print("Woe!")
else:
    print("Whee!")

#o/p:Woe!

furry=False
small=True
if furry:
    if small:
        print("It's a cat.")
    else:
        print("It's a bear!")
else:
    if small:
        print("It's a skink!")
    else:
        print("It's a human. Or a hairless bear.")
#o/p:It's a skink!

color = "puce"
if color == "red":
    print("It's a tomato")
elif color == "green":
    print("It's a green pepper")
elif color == "bee purple":
    print("I don't know what it is, but only bees can see it")
else:
    print("I've never heard of the color", color)
#o/p:I've never heard of the color puce

x = 7
x==5
#o/p:False

x==7
#o/p:True

x<5
#o/p:False

x<10
#o/p:True
#==============================================================================================

#What Is True?

some_list = []
if some_list:
    print("There's something in here")
else:
    print("Hey, it's empty!")
#o/p:Hey, it's empty!

#================================================================================================

#Repeat with while

count = 1
while count <= 5:
    print(count)
    count += 1
#o/p:
#1
#2
#3
#4
5
#==================================================================================================

#Cancel with break

while True:
    stuff = input("String to capitalize [type q to quit]: ")
    if stuff == "q":
        break
    print(stuff.capitalize())

#String to capitalize [type q to quit]: saidurga
#Saidurga
#String to capitalize [type q to quit]: chirumamilla
#Chirumamilla
#String to capitalize [type q to quit]: q

#=================================================================================================

#Skip Ahead with continue

while True:
    value = input("Integer, please [q to quit]: ")
    if value == 'q':
        break
    number = int(value)
    if number % 2 == 0:
        continue
    print(number, "squared is", number*number)

#o/p:Integer, please [q to quit]: 2
#o/p:Integer, please [q to quit]: q

#======================================================================================================

#Check break Use with else

numbers = [1, 3, 5]
position = 0
while position < len(numbers):
    number = numbers[position]
    if number % 2 == 0:
        print('Found even number', number)
        break
    position += 1
else: # break not called
    print('No even number found')
#o/p:No even number found

#=======================================================================================================

#Iterate with for

rabbits = ['Flopsy', 'Mopsy', 'Cottontail', 'Peter']
current = 0
while current < len(rabbits):
    print(rabbits[current])
    current += 1

#o/p:Flopsy
#Mopsy
#Cottontail
#Peter

for rabbit in rabbits:

    print(rabbit)

Flopsy
Mopsy
Cottontail
Peter

word = 'cat'
for letter in word:
    print(letter)

#o/p:c
#a
#t

accusation = {'room': 'ballroom', 'weapon': 'lead pipe','person': 'Col. Mustard'}
for card in accusation:
    print(card)

#o/p:room
#weapon
#person

for value in accusation.values():
    print(value)

#o/p:ballroom
#lead pipe
#Col. Mustard

for item in accusation.items():
    print(item)

#o/p:('room', 'ballroom')
#('weapon', 'lead pipe')
#('person', 'Col. Mustard')

for card, contents in accusation.items():
    print('Card', card, 'has the contents', contents)

#o/p:Card room has the contents ballroom
#Card weapon has the contents lead pipe
#Card person has the contents Col. Mustard

#====================================================================================================

#Check break Use with else

cheeses = []
for cheese in cheeses:
    print('This shop has some lovely', cheese)
    break
else:
    print('This is not much of a cheese shop, is it?')

#o/p:This is not much of a cheese shop, is it?

#======================================================================================================

#Iterate Multiple Sequences with zip()

days = ['Monday', 'Tuesday', 'Wednesday']
fruits = ['banana', 'orange', 'peach']
drinks = ['coffee', 'tea', 'beer']
desserts = ['tiramisu', 'ice cream', 'pie', 'pudding']
for day, fruit, drink, dessert in zip(days, fruits, drinks, desserts):
    print(day, ": drink", drink, "- eat", fruit, "- enjoy", dessert)

#o/p:Monday : drink coffee - eat banana - enjoy tiramisu
#Tuesday : drink tea - eat orange - enjoy ice cream
#Wednesday : drink beer - eat peach - enjoy pie

english = 'Monday', 'Tuesday', 'Wednesday'
french = 'Lundi', 'Mardi', 'Mercredi'
list(zip(english, french))
#o/p:[('Monday', 'Lundi'), ('Tuesday', 'Mardi'), ('Wednesday', 'Mercredi')]

dict(zip(english, french))
#o/p:{'Monday': 'Lundi', 'Tuesday': 'Mardi', 'Wednesday': 'Mercredi'}

#=======================================================================================================

#List Comprehensions

number_list = []
number_list.append(1)
number_list.append(2)
number_list.append(3)
number_list.append(4)
number_list.append(5)
number_list
#o/p:[1, 2, 3, 4, 5]

number_list = []
for number in range(1, 6):
    number_list.append(number)
number_list
#o/p:[1, 2, 3, 4, 5]

number_list = list(range(1, 6))
number_list
#o/p:[1, 2, 3, 4, 5]

number_list = [number-1 for number in range(1,6)]
number_list
#o/p:[0, 1, 2, 3, 4]

a_list = [number for number in range(1,6) if number % 2 == 1]
a_list

#o/p:[1, 3, 5]

rows = range(1,4)
cols = range(1,3)
for row in rows:
    for col in cols:
        print(row, col)

#o/p:1 1
#1 2
#2 1
#2 2
#3 1
#3 2

rows = range(1,4)
cols = range(1,3)
cells = [(row, col) for row in rows for col in cols]
for cell in cells:
    print(cell)

#o/p:(1, 1)
#(1, 2)
#(2, 1)
#(2, 2)
#(3, 1)
#(3, 2)

for row, col in cells:
    print(row, col)

#o/p:1 1
#1 2
#2 1
#2 2
#3 1
#3 2

#==============================================================================================

#Dictionary Comprehensions

word = 'letters'
letter_counts = {letter: word.count(letter) for letter in word}
letter_counts
#o/p:{'e': 2, 'l': 1, 'r': 1, 's': 1, 't': 2}

#=============================================================================================

#Set Comprehensions

a_set = {number for number in range(1,6) if number % 3 == 1}
a_set
#o/p:{1, 4}

#=================================================================================================

#Generator Comprehensions

number_thing = (number for number in range(1, 6))
type(number_thing)
#o/p:generator

for number in number_thing:
    print(number)

#o/p:1
#2
#3
#4
#5

#===============================================================================================

#Functions

def make_a_sound():
    print('quack')
make_a_sound()
#o/p:quack

def agree():
    return True
if agree():
    print('Splendid!')
else:
    print('That was unexpected.')
#o/p:Splendid!

def echo(anything):
    return anything + ' ' + anything
echo('Rumplestiltskin')
#o/p:'Rumplestiltskin Rumplestiltskin'

def commentary(color):
    if color == 'red':
        return "It's a tomato."
    elif color == "green":
        return "It's a green pepper."
    elif color == 'bee purple':
        return "I don't know what it is, but only bees can see it."
    else:
        return "I've never heard of the color " + color + "."
commentary('blue')
#o/p:"I've never heard of the color blue."

#==============================================================================================

#Specify Default Parameter Values

def buggy(arg, result=[]):
    result.append(arg)
    print(result)
buggy('a')
#o/p:['a']

buggy('b')
#o/p:['a', 'b', 'b']

def works(arg):
    result = []
    result.append(arg)
    return result
#o/p:works('a')
['a']
#o/p:works('b')
['b']

def nonbuggy(arg, result=None):
    if result is None:
        result = []
        result.append(arg)
    print(result)
nonbuggy('a')
#o/p:['a']
nonbuggy('b')
#o/p:['b']

#================================================================================================

#Gather Positional Arguments with *

def print_args(*args):
    print('Positional argument tuple:', args)
print_args()
#o/p:Positional argument tuple: ()

print_args(3, 2, 1, 'wait!', 'uh...')
#o/p:Positional argument tuple: (3, 2, 1, 'wait!', 'uh...')

def print_more(required1, required2, *args):
    print('Need this one:', required1)
    print('Need this one too:', required2)
    print('All the rest:', args)
print_more('cap', 'gloves', 'scarf', 'monocle', 'mustache wax')

#o/p:Need this one: cap
#Need this one too: gloves
#All the rest: ('scarf', 'monocle', 'mustache wax')

#====================================================================================================

#Gather Keyword Arguments with **

def print_kwargs(**kwargs):
    print('Keyword arguments:', kwargs)
print_kwargs(wine='merlot', entree='mutton', dessert='macaroon')
#o/p:Keyword arguments: {'wine': 'merlot', 'entree': 'mutton', 'dessert': 'macaroon'}

#===============================================================================================

#Docstrings

def echo(anything):
    'echo returns its input argument'
    return anything
help(echo)
#o/p:Help on function echo in module __main__:
#echo(anything)
 #   echo returns its input argument

print(echo.__doc__)
#o/p:echo returns its input argument

#=====================================================================================================

#Functions Are First-Class Citizens

def answer():
    print(42)
def run_something(func):
    func()
run_something(answer)
#o/p:42

type(run_something)
#o/p:function

def add_args(arg1, arg2):
#o/p:    print(arg1 + arg2)

def run_something_with_args(func, arg1, arg2):
#o/p:    func(arg1, arg2)

run_something_with_args(add_args, 5, 9)
#o/p:14

def sum_args(*args):
    return sum(args)
def run_with_positional_args(func, *args):
    return func(*args)
run_with_positional_args(sum_args, 1, 2, 3, 4)
#o/p:10

#==============================================================================================

#Inner Functions

def outer(a, b):
    def inner(c, d):
        return c + d
    return inner(a, b)
outer(2,3)
#o/p:5

#==============================================================================================

#Closures

def knights(saying):
    def inner(quote):
        return "We are the knights who say: '%s'" % quote
    return inner(saying)
knights('Ni!')
#o/p:"We are the knights who say: 'Ni!'"

def knights2(saying):
    def inner2():
        return "We are the knights who say: '%s'" % saying
    return inner2
a = knights2('Duck')
b = knights2('Hasenpfeffer')
a()
#o/p:"We are the knights who say: 'Duck'"

b()
#o/p:"We are the knights who say: 'Hasenpfeffer'"

#===================================================================================================

#Anonymous Functions: the lambda() Function
def edit_story(words, func):
    for word in words:
        print(func(word))
stairs = ['thud', 'meow', 'thud', 'hiss']
def enliven(word):
    return word.capitalize() + '!'
edit_story(stairs, enliven)
#o/p:Thud!
#Meow!
#Thud!
#Hiss!

edit_story(stairs, lambda word: word.capitalize() + '!')
#o/p:Thud!
#Meow!
#Thud!
#Hiss!

#================================================================================================

#Generators

def my_range(first=0, last=10, step=1):
    number = first
    while number < last:
        yield number
        number += step
my_range
#o/p:<function __main__.my_range>

ranger = my_range(1, 5)
ranger
#o/p:<generator object my_range at 0x7fa1d40dfa98>

for x in ranger:
    print(x)

#o/p:
#1
#2
#3
#4

#===============================================================================================

#Decorators

def document_it(func):
    def new_function(*args, **kwargs):
        print('Running function:', func.__name__)
        print('Positional arguments:', args)
        print('Keyword arguments:', kwargs)
        result = func(*args, **kwargs)
        print('Result:', result)
        return result
    return new_function
def add_ints(a, b):
    return a + b
cooler_add_ints = document_it(add_ints)
cooler_add_ints(3, 5)
#o/p:Running function: add_ints
#Positional arguments: (3, 5)
#Keyword arguments: {}
#Result: 8
#8

@document_it
def add_ints(a, b):
    return a + b
add_ints(3, 5)

#o/p:Running function: add_ints
#Positional arguments: (3, 5)
#Keyword arguments: {}
#Result: 8

#8

add_ints(3, 5)

#o/p:Running function: add_ints
#Positional arguments: (3, 5)
#Keyword arguments: {}
#Result: 8

#8

def square_it(func):
    def new_function(*args, **kwargs):
        result = func(*args, **kwargs)
        return result * result
    return new_function

@document_it
@square_it
def add_ints(a, b):
    return a + b
add_ints(3, 5)
#o/p:
#Running function: new_function
Positional arguments: (3, 5)
#Keyword arguments: {}
#Result: 64

#64

@square_it
@document_it
def add_ints(a, b):
    return a + b
add_ints(3, 5)
#o/p:
#Running function: add_ints
#Positional arguments: (3, 5)
#Keyword arguments: {}
#Result: 8

#64

#===================================================================================================

#Namespaces and Scope

animal = 'fruitbat'
def print_global():
    print('inside print_global:', animal)
print('at the top level:', animal)
#o/p:at the top level: fruitbat

print_global()
#o/p:inside print_global: fruitbat

def change_and_print_global():
    print('inside change_and_print_global:', animal)
    animal = 'wombat'
    print('after the change:', animal)
change_and_print_global()
#o/p:
#---------------------------------------------------------------------------
#UnboundLocalError                         Traceback (most recent call last)
#<ipython-input-34-3951755be21b> in <module>()
#----> 1 change_and_print_global()

#<ipython-input-33-1095d970be2a> in change_and_print_global()
 #     1 def change_and_print_global():
#----> 2     print('inside change_and_print_global:', animal)
 #     3     animal = 'wombat'
  #    4     print('after the change:', animal)

#UnboundLocalError: local variable 'animal' referenced before assignment

def change_local():
    animal = 'wombat'
    print('inside change_local:', animal, id(animal))
change_local()
#o/p:inside change_local: wombat 139854067684888

#==================================================================================================

#Handle Errors with try and except


short_list = [1, 2, 3]
position = 5
short_list[position]
#o/p:
#---------------------------------------------------------------------------
#IndexError                                Traceback (most recent call last)
#<ipython-input-16-f2887e32cb7b> in <module>()
#      1 short_list = [1, 2, 3]
##      2 position = 5
#----> 3 short_list[position]

#IndexError: list index out of range

short_list = [1, 2, 3]
position = 5
try:
    short_list[position]
except:
    print('Need a position between 0 and', len(short_list)-1, ' but got',position)

#o/p:Need a position between 0 and 2  but got 5

short_list = [1, 2, 3]
while True:
    value = input('Position [q to quit]? ')
    if value == 'q':
        break
    try:
        position = int(value)
        print(short_list[position])
    except IndexError as err:
        print('Bad index:', position)
    except Exception as other:
        print('Something else broke:', other)
#o/p:
#Position [q to quit]? 1
#2
#Position [q to quit]? 3
#Bad index: 3
#Position [q to quit]? two
#Something else broke: invalid literal for int() with base 10: 'two'
#Position [q to quit]? q

class UppercaseException(Exception):
    pass
words = ['eeenie', 'meenie', 'miny', 'MO']
for word in words:
    if word.isupper():
        raise UppercaseException(word)
#o/p:
#---------------------------------------------------------------------------
#UppercaseException                        Traceback (most recent call last)
#<ipython-input-20-566b4142c665> in <module>()
#      2 for word in words:
#     3     if word.isupper():
#----> 4         raise UppercaseException(word)

#UppercaseException: MO

#==================================================================================================

#Uses of _ and __ in Names

def amazing():
	'''This is the amazing function.
	Want to see it again?'''
	print('This function is named:', amazing.__name__)
	print('And its docstring is:', amazing.__doc__)
​amazing()
#o/p:
#This function is named: amazing
#And its docstring is: This is the amazing function.
#    Want to see it again?

​


