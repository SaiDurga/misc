def fibonacci(n):
    result = [1,1]
    while len(result) < n:
        result.append(result[-1] + result[-2])
    return result[:n]
