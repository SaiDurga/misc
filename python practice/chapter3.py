#Convert Other Data Types to Lists with list()

list('cat')
##o/p:['c', 'a', 't']

a_tuple = ('ready', 'fire', 'aim')
list(a_tuple)
##o/p:['ready', 'fire', 'aim']

birthday = '1/6/1952'
birthday.split('/')
##o/p:['1', '6', '1952']

splitme = 'a/b//c/d///e'
splitme.split('/')
##o/p:['a', 'b', '', 'c', 'd', '', '', 'e']

splitme.split('//')
##o/p:['a/b', 'c/d', '/e']
#===========================================================================

#Get an Item by Using [ offset ]

marxes = ['Groucho', 'Chico', 'Harpo']
marxes[0]
##o/p:'Groucho'

marxes[-2]
##o/p:'Chico'

#============================================================================

#Lists of Lists

small_birds = ['hummingbird', 'finch']
extinct_birds = ['dodo', 'passenger pigeon', 'Norwegian Blue']
carol_birds = [3, 'French hens', 2, 'turtledoves']
all_birds = [small_birds, extinct_birds, 'macaw', carol_birds]
all_birds
##o/p:[['hummingbird', 'finch'],
## ['dodo', 'passenger pigeon', 'Norwegian Blue'],
 #'macaw',
 #[3, 'French hens', 2, 'turtledoves']]

all_birds[0]
##o/p:['hummingbird', 'finch']

all_birds[1]
##o/p:['dodo', 'passenger pigeon', 'Norwegian Blue']

all_birds[1][0]
##o/p:'dodo'

#=============================================================================

#Change an Item by [ offset ]

marxes = ['Groucho', 'Chico', 'Harpo']
marxes[2] = 'Wanda'
marxes
##o/p:['Groucho', 'Chico', 'Wanda']

#==============================================================================

#Get a Slice to Extract Items by Offset Range

marxes = ['Groucho', 'Chico', 'Harpo']
marxes[0:2]
##o/p:['Groucho', 'Chico']

marxes[::2]
##o/p:['Groucho', 'Harpo']

marxes[::-2]
##o/p:['Harpo', 'Groucho']

marxes[::-1]
##o/p:['Harpo', 'Chico', 'Groucho']

#==================================================================================

#Add an Item to the End with append()

marxes.append('Zeppo')
marxes
##o/p:['Groucho', 'Chico', 'Harpo', 'Zeppo']

#==================================================================================

#Combine Lists by Using extend() or +=

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo']
others = ['Gummo', 'Karl']
marxes.extend(others)
marxes
##o/p:['Groucho', 'Chico', 'Harpo', 'Zeppo', 'Gummo', 'Karl']

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo']
others = ['Gummo', 'Karl']
marxes += others
marxes
##o/p:['Groucho', 'Chico', 'Harpo', 'Zeppo', 'Gummo', 'Karl']

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo']
others = ['Gummo', 'Karl']
marxes.append(others)
marxes
##o/p:['Groucho', 'Chico', 'Harpo', 'Zeppo', ['Gummo', 'Karl']]

#====================================================================================

#Add an Item by Offset with insert()

marxes.insert(3, 'Gummo')
marxes
##o/p:['Groucho', 'Chico', 'Harpo', 'Gummo', 'Zeppo', ['Gummo', 'Karl']]

#====================================================================================

#Delete an Item by Offset with del

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo']
del marxes[-1]
marxes
##o/p:['Groucho', 'Chico', 'Harpo']

marxes[2]
##o/p:'Harpo'

del marxes[2]
marxes
##o/p:['Groucho', 'Chico']

#===================================================================================

#Delete an Item by Value with remove()
marxes = ['Groucho', 'Chico', 'Harpo', 'Gummo', 'Zeppo']
marxes.remove('Gummo')
marxes
##o/p:['Groucho', 'Chico', 'Harpo', 'Zeppo']

#===================================================================================
#Get an Item by Offset and Delete It by Using pop()

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo']
marxes.pop()
##o/p:'Zeppo'

marxes
##o/p:['Groucho', 'Chico', 'Harpo']

marxes.pop(1)
##o/p:'Chico'

marxes
##o/p:['Groucho', 'Harpo']

#==================================================================================

#Find an Item’s Offset by Value with index()

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo']
marxes.index('Chico')
##o/p:1

#===================================================================================

#Test for a Value with in

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo']
'Groucho' in marxes
##o/p:True

'Bob' in marxes
##o/p:False

#===================================================================================
#Count Occurrences of a Value by Using count()

marxes = ['Groucho', 'Chico', 'Harpo']
marxes.count('Harpo')
##o/p:1

marxes.count('Bob')
##o/p:0

#===================================================================================

#Convert to a String with join()

marxes = ['Groucho', 'Chico', 'Harpo']
', '.join(marxes)
##o/p:'Groucho, Chico, Harpo'

friends = ['Harry', 'Hermione', 'Ron']
separator = ' * '
joined = separator.join(friends)
joined
##o/p:'Harry * Hermione * Ron'

separated = joined.split(separator)
separated
##o/p:['Harry', 'Hermione', 'Ron']

separated == friends
##o/p:True

#===================================================================================

#Reorder Items with sort()

marxes = ['Groucho', 'Chico', 'Harpo']
sorted_marxes = sorted(marxes)
sorted_marxes
##o/p:['Chico', 'Groucho', 'Harpo']

marxes
##o/p:['Groucho', 'Chico', 'Harpo']

marxes.sort()
marxes
##o/p:['Chico', 'Groucho', 'Harpo']

numbers = [2, 1, 4.0, 3]
numbers.sort()
numbers
##o/p:[1, 2, 3, 4.0]

numbers = [2, 1, 4.0, 3]
numbers.sort(reverse=True)
numbers
##o/p:[4.0, 3, 2, 1]
#===============================================================================
#Assign with =, Copy with copy()
a = [1, 2, 3]
b=a
a[0] = 'surprise'
a
##o/p:['surprise', 2, 3]

b
##o/p:['surprise', 2, 3]

a = [1, 2, 3]
b = a.copy()
c = list(a)
d = a[:]
a[0] = 'integer lists are boring'
a
##o/p:['integer lists are boring', 2, 3]

b
##o/p:[1, 2, 3]

c
##o/p:[1, 2, 3]

d
##o/p:[1, 2, 3]

#=========================================================================================
#Create a Tuple by Using ()

marx_tuple = ('Groucho', 'Chico', 'Harpo')
marx_tuple
##o/p:('Groucho', 'Chico', 'Harpo')

a, b, c = marx_tuple
a
##o/p:'Groucho'

b
##o/p:'Chico'

c
##o/p:'Harpo'

password = 'swordfish'
icecream = 'tuttifrutti'
password, icecream = icecream, password
password
##o/p:'tuttifrutti'

icecream
##o/p:'swordfish'

marx_list = ['Groucho', 'Chico', 'Harpo']
tuple(marx_list)
##o/p:('Groucho', 'Chico', 'Harpo')
#============================================================================================

#Dictionaries
#Create with {}
bierce = {"day": "A period of twenty-four hours, mostly misspent","positive": "Mistaken at the top of one's voice",

          "misfortune": "The kind of fortune that never misses"}
bierce
##o/p:{'day': 'A period of twenty-four hours, mostly misspent',
# 'misfortune': 'The kind of fortune that never misses',
 #'positive': "Mistaken at the top of one's voice"}
#===============================================================================================

#Convert by Using dict()

lol = [ ['a', 'b'], ['c', 'd'], ['e', 'f'] ]
dict(lol)
##o/p:{'a': 'b', 'c': 'd', 'e': 'f'}

lot = [ ('a', 'b'), ('c', 'd'), ('e', 'f') ]
dict(lot)
##o/p:{'a': 'b', 'c': 'd', 'e': 'f'}

tol = ( ['a', 'b'], ['c', 'd'], ['e', 'f'] )
dict(tol)
##o/p:{'a': 'b', 'c': 'd', 'e': 'f'}

los = [ 'ab', 'cd', 'ef' ]
dict(los)
##o/p:{'a': 'b', 'c': 'd', 'e': 'f'}

tos = ( 'ab', 'cd', 'ef' )
dict(tos)
##o/p:{'a': 'b', 'c': 'd', 'e': 'f'}
#====================================================================================

#Add or Change an Item by [ key ]

pythons = {'Chapman': 'Graham', 'Cleese': 'John', 'Idle': 'Eric', 'Jones': 'Terry','Palin': 'Michael'}
pythons
##o/p:{'Chapman': 'Graham',
# 'Cleese': 'John',
 #'Idle': 'Eric',
 #'Jones': 'Terry',
 #'Palin': 'Michael'}

pythons['Gilliam'] = 'Gerry'
pythons
##o/p:{'Chapman': 'Graham',
# 'Cleese': 'John',
# 'Gilliam': 'Gerry',
# 'Idle': 'Eric',
# 'Jones': 'Terry',
# 'Palin': 'Michael'}

pythons['Gilliam'] = 'Terry'
pythons
##o/p:{'Chapman': 'Graham',
# 'Cleese': 'John',
# 'Gilliam': 'Terry',
# 'Idle': 'Eric',
# 'Jones': 'Terry',
# 'Palin': 'Michael'}

#========================================================================================

#Combine Dictionaries with update()

first = {'a': 1, 'b': 2}
second = {'b': 'platypus'}
first.update(second)
first
##o/p:{'a': 1, 'b': 'platypus'}

#=========================================================================================

#Delete an Item by Key with del

del pythons['Idle']
pythons
##o/p:{'Chapman': 'Graham',
# 'Cleese': 'John',
# 'Gilliam': 'Terry',
# 'Jones': 'Terry',
# 'Palin': 'Michael'}

#=========================================================================================

#Delete All Items by Using clear()

pythons.clear()
pythons
##o/p:{}pythons = {'Chapman': 'Graham', 'Cleese': 'John',
#'Jones': 'Terry', 'Palin': 'Michael'}

#=========================================================================================

#Test for a Key by Using in

'Chapman' in pythons
##o/p:True

'Gilliam' in pythons
##o/p:False

#=========================================================================================

#Get an Item by [ key ]

pythons.get('Cleese')
##o/p:'John'

pythons.get('Marx', 'Not a Python')
##o/p:'Not a Python'

#===========================================================================================

#Get All Keys by Using keys()

signals = {'green': 'go', 'yellow': 'go faster', 'red': 'smile for the camera'}
signals.keys()
##o/p:dict_keys(['green', 'yellow', 'red'])

#==========================================================================================

#Get All Values by Using values()

signals.values()
##o/p:dict_values(['go', 'go faster', 'smile for the camera'])

#============================================================================================
#Get All Key-Value Pairs by Using items()

signals.items()
##o/p:dict_items([('green', 'go'), ('yellow', 'go faster'), ('red', 'smile for the camera')])

#============================================================================================

#Assign with =, Copy with copy()

signals = {'green': 'go', 'yellow': 'go faster', 'red': 'smile for the camera'}
save_signals = signals
signals['blue'] = 'confuse everyone'
save_signals
##o/p:{'blue': 'confuse everyone',
# 'green': 'go',
# 'red': 'smile for the camera',
# 'yellow': 'go faster'}

signals = {'green': 'go', 'yellow': 'go faster', 'red': 'smile for the camera'}
original_signals = signals.copy()
signals['blue'] = 'confuse everyone'
signals
##o/p:{'blue': 'confuse everyone',
# 'green': 'go',
# 'red': 'smile for the camera',
 #'yellow': 'go faster'}

original_signals
##o/p:{'green': 'go', 'red': 'smile for the camera', 'yellow': 'go faster'}

#========================================================================================
#Convert from Other Data Types with set()

set( 'letters' )
##o/p:{'e', 'l', 'r', 's', 't'}

set( {'apple': 'red', 'orange': 'orange', 'cherry': 'red'} )
##o/p:{'apple', 'cherry', 'orange'}

#======================================================================================
#Test for Value by Using in

drinks = {'martini': {'vodka', 'vermouth'},'black russian': {'vodka', 'kahlua'},

          'white russian': {'cream', 'kahlua', 'vodka'},'manhattan': {'rye', 'vermouth', 'bitters'},

          'screwdriver': {'orange juice', 'vodka'}}

for name, contents in drinks.items():
	if 'vodka' in contents:
		print(name)

##o/p:martini
#black russian
#white russian
#screwdriver

#=======================================================================================

#Combinations and Operators

for name, contents in drinks.items():
	if 'vodka' in contents and not contents & {'vermouth', 'cream'}:
		print(name)

##o/p:black russian
#screwdriver

a={1,2}
b={2,3}
a&b
##o/p:{2}

a.intersection(b)
##o/p:{2}

a|b
##o/p:{1, 2, 3}

a.union(b)
##o/p:{1, 2, 3}

a-b
##o/p:{1}

a.difference(b)
##o/p:{1}

a^b
##o/p:{1, 3}

a.symmetric_difference(b)
##o/p:{1, 3}

a <= b
##o/p:False

a.issubset(b)
##o/p:False

a >= b
##o/p:False

a.issuperset(b)
##o/p:False

​


