def arm_num(n):
    s = 0
    num = n
    while (num>0) :
        rem = num%10
        s = s + (rem*rem*rem)
        num = num//10
    if s==n :
        print(n,'is an armstrong number')
    else:
        print("no")


def armstrong_num(start,stop):
    for n in range (start,stop+1):
        s=0
        num=n
        while(num>0):
            rem = num%10
            s = s + (rem*rem*rem)
            num =num//10
        if s==n :
            print(n)

def armstrong_number(start,stop):
    for n in range (start,stop+1):
        count = 0
        dig = n
        while(dig != 0):
            dig = dig//10
            count = count+1
        s = 0
        num = n
        while (num>0):
            rem = num%10
            s = s+(rem**count)
            num=num//10
        if s==n :
            print(n)


print(arm_num(3))