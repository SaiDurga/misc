1))            ##MAP##

def map1(f):
    def mapped(lst):
        result = []
        for e in lst:
            result.append(f(e))
        return result
    return mapped


def inc(n):
    return n+1


map1(inc)([1,2,3,4,5])     o/p: [2, 3, 4, 5, 6]
map1(squ)([1,2,3,4,5])     olp: [1, 4, 9, 16, 25]

----------------------------------------------------------
2))             ##FILTER##

def filter(pred,lst):
    filtered = []
    for e in lst:
        if pred(e) :
            filtered.append(e)
    return filtered

filter(even,([1,3,4,2,5,6,7,8]))     o/p:[4, 2, 6, 8]
filter(odd,([1,3,4,2,5,6,7,8]))      o/p:[1, 3, 5, 7]

-------------------------------------------------------------
3))            ##DISTINCT##

def distinct(lst):
    result = []
    result.append(lst[0])
    for e in lst:
        if e not in result :
            result.append(e)
    return result

distinct([1,1,1,2,3,3,3,4,5,5])   o/p: [1,2,3,4,5]
distinct([1,2,3,2,3,4,3,4,5,6,7,8,3,4,5,2,3,7,6,8)  o/p:[1,2,3,4,5,6,7,8]
-------------------------------------------------------------
4))            ##DEDUPE##

def dedupe(lst):
    result = []
    l = len(lst)
    for i in range(0,l) :
        if (lst[i] != lst[i-1]) :
            result.append(lst[i])
    return result

dedupe([1,2,3,3,3,1,1,6,6,6,2,2])  o/p: [1, 2, 3, 1, 6, 2]
dedupe([1,2,3,3,1,4,4,5])          o/p: [1, 2, 3, 1, 4, 5]
--------------------------------------------------------------
5))            ##CYCLE##

def cycle(n):
    def cycled(lst):
        result = []
        while (len(result)<n) :
            for e in lst:
                if len(result) == n :
                    return result
                else:
                    result.append(e)
    return cycled

cycle(10)([0,1,2])  o/p:[0, 1, 2, 0, 1, 2, 0, 1, 2, 0]
cycle(1)([1,2,3])   o/p:[1]
cycle(5)([1,2])     o/p:[1,2,1,2,1]
-------------------------------------------------------------
6))          ##INTERLEAVE##

def interleave(lst1,lst2):
    l1 = len(lst1)
    l2 = len(lst2)
    if l1<l2 :
        l = l1
    else:
        l = l2
    result = []
    for i in range(0,l):
        result.append(lst1[i])
        result.append(lst2[i])
    return result

interleave([1,2,3],[4,5,6])    o/p:[1, 4, 2, 5, 3, 6]
interleave([1,2,3],[5,6,7,8,9])o/p:[1, 5, 2, 6, 3, 7]
interleave([1,2,3,4,5,6],[7,8])o/p:[1, 7, 2, 8]

---------------------------------------------------------------
7))           ##INTERPOSE##

def interpose(s):
    def interposed(lst):
        result = []
        l = len(lst)
        for e in lst:
            result.append(e)
            if (len(result)<((2*l)-1)) :
                result.append(s)
        return result
    return interposed

interpose('a')([1,2,3,4,5])   o/p:[1, 'a', 2, 'a', 3, 'a', 4, 'a', 5]
interpose([1,2])([1,2,3,4,5,6])o/p:[1, [1, 2], 2, [1, 2], 3, [1, 2], 4, [1, 2], 5, [1, 2], 6]
-----------------------------------------------------------------
8))           ##TAKE##

def take(n):
    def taken(lst):
        result = []
        for e in lst:
            if(len(result)<n):
                result.append(e)
                
        return result
    return taken

take(10)([1,4,7,89,34]) o/p: [1, 4, 7, 89, 34]
take(3)([1,2,3,4,5])    o/p: [1, 2, 3]
take(0)([1,2,3])        o/p: []

-----------------------------------------------------------------
9))           ##TAKEWHILE##

def takewhile(f):
    def takenwhile(lst):
        result = []
        for e in lst:
            if f(e):
                result.append(e)
            else:
                return result
    return takenwhile

def even(n):
    if (n%2) == 0 :
        return n

takewhile(even)([2,4,6,1,8,12,3,4]) o/p: [2,4,6]
takewhile(even)([1,2,4,6,1,8,12,3,4]) o/p: []

----------------------------------------------------------------------
10))           ##CONCAT##

def concat(lst1,lst2):
    result = lst1
    for e in lst2:
        result.append(e)
    return result

def concat(lst1,lst2):
    result = []
    for e in lst1:
        result.append(e)
    for e in lst2:
        result.append(e)
    return result

concat([1,2,3],[4,5,6])     o/p:[1, 2, 3, 4, 5, 6]
concat([1,2,3],[4,[5,6],7]) o/p:[1, 2, 3, 4, [5, 6], 7]
--------------------------------------------------------------------------
11))            ##FLATTEN##

def flatten(lst):
    res = []
    for e in lst:
        if(type(e) == list):
            for i in e:
                if (type(i) == list):
                    for j in i:
                        res.append(j)
                else:
                    res.append(i)
        else:
            res.append(e)
    return res

flatten(([1,2,3],[4,"sai",[5,6]]))  o/p:[1, 2, 3, 4, 'sai', 5, 6]
flatten(([1,2,3],[4,5,6,7]))        o/p:[1, 2, 3, 4, 5, 6, 7]
flatten(([1,2,3],[4,[5,6],7]))      o/p:[1, 2, 3, 4, 5, 6, 7]

------------------------------------------------------------------------
12))         ##SPLIT-AT##

def splitat(n,lst):
    lst1 =[]
    lst2 =[]
    if n in lst:
        i = lst.index(n)
        for e in range(0,i+1):
            lst1.append(lst[e])
        for e in range(i+1,len(lst)):
            lst2.append(lst[e])
    else:
        lst1 = lst
    result = [lst1,lst2]
    return result

splitat(3,[1,2,3,4,5])     o/p: [[1, 2, 3], [4, 5]]
splitat(2,[0,1,2,3,4,5])   o/p: [[0, 1, 2], [3, 4, 5]]
splitat(0,[1,2,3,4,5])     o/p: [[1, 2, 3, 4, 5], []]

------------------------------------------------------------------------
13))             ##SPLIT-WITH##

def takewhile(f):
    def takenwhile(lst):
        result = []
        for e in lst:
            if f(e):
                result.append(e)
            else:
                return result
    return takenwhile

def dropwhile(pred,lst):
    for e in range(0,len(lst)):
         if pred(lst[e]) :
            if not pred(lst[e+1]) :
                return lst[e+1:]
         else:
            return lst

def split_with(pred,lst):
    return((takewhile(pred)(lst)),(dropwhile(pred,lst)))

split_with(even,([1,2,3,4,5,6]))  o/p:([], [1, 2, 3, 4, 5, 6])
split_with(even,([4,6,2,3,4,5,6]))o/p:([4, 6, 2], [3, 4, 5, 6])

--------------------------------------------------------------------------
14))             ##REVERSE##

def reverse(lst):
    result = []
    l = len(lst)
    for i in range(0,l):
        result.append(lst[(l-1)-i])
    return result


reverse([1,3,5,7,9,2,8])  o/p: [8, 2, 9, 7, 5, 3, 1]
reverse([63,68,86,72,73]) o/p: [73, 72, 86, 68, 63]
-----------------------------------------------------------------------------
15))            ##REDUCE##

def reduce(bf,init):
    def reduced(lst):
        result = init
        for e in lst:
            result = bf(result,e)
        return result
    return reduced

def sum(a,b):
    return a+b

reduce(sum,0)([1,2,3,4,5])      o/p:15
reduce(sum,5)([1,2,3,4,5])      o/p:20
reduce(sum,0)(range(10))        o/p:45
-----------------------------------------------------------------------------------
16))          ##PARTITION##
def partition(n,lst):
    result = []
    for i in range(0,(len(lst)//n)):
        result.append(lst[(i*n):((i+1)*n)])
    return result

def partition1(n,s,lst):
    result = []
    if s > n:
        l = len(lst)//s + 1
    else:
        l = (len(lst)//s)
    for i in range(l):
        if len(lst[(i*s):((i*s)+n)]) == n:
            result.append(lst[(i*s):((i*s)+n)])
    return result

def partition2(n,s,p,lst):
    lst1 = lst + p
    result = []
    if s > n:
        l = len(lst)//s + 1
    else:
        l = (len(lst)//s)
    for i in range(l):
        if len(lst[(i*s):((i*s)+n)]) == n :
                result.append(lst[(i*s):((i*s)+n)])
        else:
            if len(p) != 0 and len(lst[(i*s):((i*s)+n)]) != 0:
                 result.append(lst1[(i*s):((i*s)+n)])
    return result


partition2(3,5,[],[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])    o/p:[[1, 2, 3], [6, 7, 8], [11, 12, 13]]
partition2(3,5,["a"],[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]) o/p:[[1, 2, 3], [6, 7, 8], [11, 12, 13]]
partition2(3,5,["a"],[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]) o/p:[[1, 2, 3], [6, 7, 8], [11, 12, 13], [16, 'a']]
partition2(3,5,["a","b","c"],[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16])  o/p:[[1, 2, 3], [6, 7, 8], [11, 12, 13], [16, 'a', 'b']]


---------------------------------------------------------------
17))               ##PARTITION_ALL##
def partition_all(n,lst):
    result = []
    for i in range(0,len(lst)//n+1):
        result.append(lst[(i*n):((i+1)*n)])
    return result

partition_all(5,([1,2,3,4,5,6,7,8,9,10,5,7,8]))   o/p:[[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [5, 7, 8]]
partition_all(3,([3,4,2,6,8,2,5,9,8,7]))          o/p:[[3, 4, 2], [6, 8, 2], [5, 9, 8], [7]]
------------------------------------------------------------------------------------
18))		##sort##

def sort(lst):
    for i in range(len(lst)-1):
        for j in range(i + 1, len(lst)):
            if (lst[i] > lst[j]):
                (lst[i],lst[j]) = (lst[j],lst[i])
    return lst

sort([8,7,6,5,4,3])   o/p:[3, 4, 5, 6, 7, 8]
sort([1,3,4,2,5,7,6,4,8,7,9])  o/p: [1, 2, 3, 4, 4, 5, 6, 7, 7, 8, 9]

--------------------------------------------------------------------------------------------
19))         ##mapcat##

def mapcat(f,lst):
    res = []
    for e in lst:
        res.append(f(e))
    return flatten(res)

mapcat(reverse,(([1,2,3],[4,5,6,7],[8,9])))    o/p: [3, 2, 1, 7, 6, 5, 4, 9, 8]
mapcat(inc,(([1,2,3],[4,5,6,7],[8,9])))        o/p: [2, 3, 4, 5, 6, 7, 8, 9, 10]

-------------------------------------------------------------------------------------------------
