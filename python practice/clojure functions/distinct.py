def distinct(lst):
    result = []
    result.append(lst[0])
    for e in lst:
        if e not in result :
            result.append(e)
    return result
