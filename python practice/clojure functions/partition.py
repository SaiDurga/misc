def partition(n,lst):
    result = []
    for i in range(0,(len(lst)//n)):
        result.append(lst[(i*n):((i+1)*n)])
    return result
