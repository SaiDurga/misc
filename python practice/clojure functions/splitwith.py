def takewhile(f):
    def takenwhile(lst):
        result = []
        for e in lst:
            if f(e):
                result.append(e)
            else:
                return result
    return takenwhile

def dropwhile(pred,lst):
    for e in range(0,len(lst)):
         if pred(lst[e]) :
            if not pred(lst[e+1]) :
                return lst[e+1:]
         else:
            return lst

def split_with(pred,lst):
    return((takewhile(pred)(lst)),(dropwhile(pred,lst)))
