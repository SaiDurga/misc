def takewhile(f):
    def takenwhile(lst):
        result = []
        for e in lst:
            if f(e):
                result.append(e)
            else:
                return result
    return takenwhile

def even(n):
    if (n%2) == 0 :
        return n

takewhile(even)([2,4,6,1,8,12,3,4])
