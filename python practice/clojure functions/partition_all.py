def partition_all(n,lst):
    result = []
    for i in range(0,len(lst)//n+1):
        result.append(lst[(i*n):((i+1)*n)])
    return result

