def interpose(s):
    def interposed(lst):
        result = []
        l = len(lst)
        for e in lst:
            result.append(e)
            if (len(result)<((2*l)-1)) :
                result.append(s)
        return result
    return interposed
