def filter(pred,lst):
    filtered = []
    for e in lst:
        if pred(e) :
            filtered.append(e)
    return filtered
