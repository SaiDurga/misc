def reduce(bf,init):
    def reduced(lst):
        result = init
        for e in lst:
            result = bf(result,e)
        return result
    return reduced
