print("This interactive snippet works.")


import sys
print('Program arguments:', sys.argv)

python test2.py
##Program arguments: ['test2.py']
python test2.py tra la la
##Program arguments: ['test2.py', 'tra', 'la', 'la']



##weatherman.py

import report
description = report.get_description()
print("Today's weather:", description)
##------------------------------------------------------------
import report as wr
description = wr.get_description()
print("Today's weather:", description)

##-----------------------------------------------------------
from report import get_description
description = get_description()
print("Today's weather:", description)

##------------------------------------------------------------
from report import get_description as do_it
description = do_it()
print("Today's weather:", description)

##============================================================

##report.py

def get_description():
	from random import choice
	possibilities = ['rain', 'snow', 'sleet', 'fog', 'sun', 'who knows']
	return choice(possibilities)


python weatherman.py
##Today's weather: who knows
python weatherman.py
##Today's weather: sun
python weatherman.py
##Today's weather: sleet

def get_description():
import random
possibilities = ['rain', 'snow', 'sleet', 'fog', 'sun', 'who knows']
return random.choice(possibilities)

##==================================================================

import sys
for place in sys.path:
    print(place)

##o/p:

##/home/sai/anaconda3/lib/python36.zip
##/home/sai/anaconda3/lib/python3.6
##/home/sai/anaconda3/lib/python3.6/lib-dynload
##/home/sai/anaconda3/lib/python3.6/site-packages
##/home/sai/anaconda3/lib/python3.6/site-packages/Sphinx-1.5.6-py3.6.egg
##/home/sai/anaconda3/lib/python3.6/site-packages/setuptools-27.2.0-py3.6.egg
##/home/sai/anaconda3/lib/python3.6/site-packages/IPython/extensions
##/home/sai/.ipython


##======================================================================
##Count Items with Counter()

from collections import Counter
breakfast = ['spam', 'spam', 'eggs', 'spam','eggs','butter']
breakfast_counter = Counter(breakfast)
breakfast_counter

##o/p:  Counter({'butter': 1, 'eggs': 2, 'spam': 3})

breakfast_counter.most_common()
##o/p:   [('spam', 3), ('eggs', 2), ('butter', 1)]

breakfast_counter.most_common(1)
##o/p:  [('spam', 3)]

breakfast_counter.most_common(2)
##o/p:  [('spam', 3), ('eggs', 2)]

breakfast_counter.most_common(3)
##o/p:  [('spam', 3), ('eggs', 2), ('butter', 1)]

lunch = ['eggs', 'eggs', 'bacon']
lunch_counter = Counter(lunch)
lunch_counter

##o/p:  Counter({'bacon': 1, 'eggs': 2})

lunch_counter
##o/p:  Counter({'bacon': 1, 'eggs': 2})

breakfast_counter + lunch_counter
##o/p:	Counter({'bacon': 1, 'butter': 1, 'eggs': 4, 'spam': 3})

breakfast_counter - lunch_counter
##o/p:	Counter({'butter': 1, 'spam': 3})

lunch_counter - breakfast_counter
##o/p:	Counter({'bacon': 1})

breakfast_counter & lunch_counter
##o/p:	Counter({'eggs': 2})

breakfast_counter | lunch_counter
##o/p:	Counter({'bacon': 1, 'butter': 1, 'eggs': 2, 'spam': 3})

##=============================================================================

##Order by Key with OrderedDict()

quotes = {'Moe': 'A wise guy, huh?',
'Larry': 'Ow!', 'Curly': 'Nyuk nyuk!'}
for e in quotes:
	print(e)
##o/p:

##Curly
##Moe
##Larry


from collections import OrderedDict
quotes = OrderedDict([('Moe', 'A wise guy, huh?'),('Larry', 'Ow!'),('Curly', 'Nyuk nyuk!')])
for e in quotes:
	print(e)
##o/p:

##Moe
##Larry
##Curly

##===============================================================================
##Deque

def palindrome(word):
    from collections import deque
    dq = deque(word)
    while len(dq) > 1:
        if dq.popleft() != dq.pop():
            return False
    return True

##	(or)

def another_palindrome(word):
    return word == word[::-1]


##o/p:

##palindrome('a')		true
##palindrome('radar')		true
##palindrome('abcba')		true
##palindrome('saidurga')	false


##=================================================================================
##Iterate over Code Structures with itertools

import itertools
for item in itertools.chain([1, 2], ['a', 'b']):
    print(item)

##o/p:
##1
##2
##a
##b

import itertools
for e in itertools.chain([63,68],['a','b','c','d'],['sai','durga','chirumamilla']):
    print(e)

##o/p:
##63
##68
##a
##b
##c
##d
##sai
##durga
##chirumamilla

import itertools
for item in itertools.accumulate([1, 2, 3, 4]):
    print(item)

##o/p:
##1
##3
##6
##10


import itertools
def multiply(a,b):
    return a*b
for e in itertools.accumulate([1,2,3,4,5],multiply):
    print(e)

##o/p:
##1
##2
##6
##24
##120


##======================================================================================
##Print Nicely with pprint()

from pprint import pprint
from collections import OrderedDict
quotes = OrderedDict([('Moe', 'A wise guy, huh?'),('Larry', 'Ow!'),('Curly', 'Nyuk nyuk!'),])


print(quotes)
##OrderedDict([('Moe', 'A wise guy, huh?'), ('Larry', 'Ow!'), ('Curly', 'Nyuk nyuk!')])

pprint(quotes)
##OrderedDict([('Moe', 'A wise guy, huh?'),
             ('Larry', 'Ow!'),
             ('Curly', 'Nyuk nyuk!')])


##=================================================================================




