def freq(lst):
    freq = {}
    for e in lst:
        freq[e] = freq.get(e,0) + 1
    return freq

----------------------------------------------

def concat(lst1,e):
    result = lst1
    result.append(e)
    return result

def index(lst):
    r = {}
    for i in range(len(lst)):
        r[lst[i]] = concat(r.get(lst[i],[]),i)
    return r
